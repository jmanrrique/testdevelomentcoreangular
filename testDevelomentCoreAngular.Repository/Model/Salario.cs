﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace testDevelomentCoreAngular.Repository.Model
{
    class Salario
    {
        [Key]
        public int Id { get; set; }

        public int Year { get; set; }

        public int Month { get; set; }

        [MaxLength(10)]
        public string EmployeeCode { get; set; }

        [MaxLength(150)]
        public string EmployeeName { get; set; }

        [MaxLength(150)]
        public string EmployeeSurname { get; set; }

        public int Grade { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime Birthday { get; set; }

        [MaxLength(10)]
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Use numbers only please")]
        public string Identification { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal BaseSalary { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal ProductionBonus { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal CompensationBonus { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal Commission { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal Contributions { get; set; }

        public string Office { get; set; }
        public string Division { get; set; }
        public string Position { get; set; }
    }
}
