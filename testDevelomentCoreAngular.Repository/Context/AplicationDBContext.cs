﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testDevelomentCoreAngular.Repository.Model;

namespace testDevelomentCoreAngular.Repository.Context
{
    public class AplicationDBContext : DbContext
    {
        
        DbSet<Salario> Salario { get; set; }

        public AplicationDBContext(DbContextOptions<AplicationDBContext> option) : base(option)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            DateTime fecha = DateTime.Now;
            fecha.AddYears(-1);
            int countEmpleado = 0;
            var rand = new Random();
            for (int empleado = 10; empleado < 12; empleado++)
            {
                countEmpleado++;
                DateTime Today = fecha;
                DateTime BeginDate = fecha;
                BeginDate.AddMonths(empleado);

                for (int Month = 1; Month < 7; Month++)
                {
                    Today.AddMonths(1);
                    modelBuilder.Entity<Salario>().HasData(
                        new Salario
                        {
                            Id = countEmpleado,
                            Year = fecha.Year,
                            Month = Today.Month,
                            EmployeeCode = empleado.ToString(),
                            EmployeeName = "TestName" + empleado,
                            EmployeeSurname = "TesttSurname" + empleado,
                            Grade = empleado,
                            BeginDate = BeginDate,
                            Birthday = BeginDate,
                            Identification = "456421" + empleado,
                            BaseSalary = (decimal)(rand.NextDouble() * 10000),//revisar
                            ProductionBonus = (decimal)(rand.NextDouble() * 10000),//revisar
                            CompensationBonus = (decimal)(rand.NextDouble() * 10000),//revisar
                            Commission = (decimal)(rand.NextDouble() * 10000),//revisar
                            Contributions = (decimal)(rand.NextDouble() * 10000),//revisar
                            Office = "C",
                            Division = "OPERATION",
                            Position = "CARGO MANAGER"
                        });
                }
            }
        }
    }
}
